/* global define */
define([
  'backbone',
  'bootstrap_dropdown',
  'application/scope',
  'helpers/responsive',
  'application/views/_parallex',
], function(Backbone, bootstrap, scope, responsive, parallex) {
  'use strict';
  // scope.skrollr = new parallex(el: $('.hideme'));
  return scope;
});
